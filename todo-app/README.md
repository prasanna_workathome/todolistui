
## Prerequisite 
  To run projects locally below are the prerequisite
  -------------------------------------------------------------------------------------
  * latest Node js needs to be installed to be used as npm package Manager 
  * Pull code from bitbucket public repo ` https://bitbucket.org/prasanna_hach/todolistui/src/master/`
  * Run this command to pull latest code using this `git clone https://prasanna_hach@bitbucket.org/prasanna_hach/todolistui.git`
  * Open code in Visual studio code or any other editor 
  * Run `npm install -g @angular/cli` to install Angular or Run  `npm install ` as all dependencies configured in package.json(Assumption npm packagemanager alreday installed)
  * More Info related to local Angular set up please navigate to angular official website `https://angular.io/guide/setup-local`
 # TodoApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## About Application Architecture ## 

## models :
* Modles folder has class  to-do-list-item and todo-details to hold both List and Details API Response and UI Data binding

## Components 
* todo-list and todo-details are two comonents which used for List ,Details and Add Screen in the UI
* Both todo-list and todo-details are called in app component

## Services :
There are two services in application
* todolist.service used to interact with API and get data to Bind in Angular UI
* todolist-share.service is used to share data between angular list and details component
* Angular UI input field is used css and Angular material component along with angular Forms

## Services :
* unit tests are written for API Service and validate components



