import { TestBed } from '@angular/core/testing';
import { ToDoListItem } from './models/to-do-list-item';
import { TodolistService } from './todolist.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpClientModule  } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('TodolistService', () => {
  let apiURL: 'https://localhost:44327/api/ToDoList';
  let service: TodolistService;
  let httpClient : HttpClient
  let httpMock: HttpTestingController;
  let httpTestingController: HttpTestingController;
  let testToDoList: ToDoListItem;
  let result : ToDoListItem[];
  let postResult : ToDoListItem;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,RouterTestingModule,HttpClientModule],
      providers: [TodolistService]
    });
    service = TestBed.inject(TodolistService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    httpMock = TestBed.get(HttpTestingController);
    testToDoList = {
      id: '1',
      name : 'Test Data',
      isCompleted: false,
      createdById: 'Test Service',
      createdOn: new Date(),
      modifiedById: 'Test Service',
      modifiedOn: new Date()
    };
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('be able to retrieve ToDo List',() =>{
  service.getAllToDoList().subscribe(t => {
    result = t;
   });
   const req = httpTestingController.expectOne({
    method: "GET",
    url: apiURL
  });
  req.flush([testToDoList]);
  expect(result[0]).toEqual(testToDoList);
  });
  
  it('be able to Create ToDo List',() =>{
    service.AddToDoList(testToDoList).subscribe(t => {
      postResult = t;
     });
     const req = httpTestingController.expectOne({
      method: "POST",
      url: apiURL
    });
    req.flush(testToDoList);
    expect(postResult).toEqual(testToDoList);
    });
 afterEach(() => {
  httpMock.verify();
});
});
