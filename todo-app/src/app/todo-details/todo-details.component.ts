import { Component, Input, OnInit } from '@angular/core';
import { TodoDetails } from '../models/todo-details';
import { TodolistService } from '../todolist.service';
import {FormControl, Validators, FormBuilder} from '@angular/forms';
import { ToDoListItem } from '../models/to-do-list-item';
import { TodolistShareService } from '../todolist-share.service';

@Component({
  selector: 'app-todo-details',
  templateUrl: './todo-details.component.html',
  styleUrls: ['./todo-details.component.css']
})
export class TodoDetailsComponent implements OnInit {
  @Input() ItemId: any;
  item!: TodoDetails;
  createdEntity !: ToDoListItem;
  isDetailsView!: boolean;
  isAddView!: boolean;
  form = this.fb.group({
    name: ['',
    Validators.compose([
      Validators.required,
      Validators.minLength(2)])]
  });
  constructor(private toDoListService:TodolistService,private fb: FormBuilder,private todolistShareService:TodolistShareService) { 
  }
  ngOnInit() {
    this.isDetailsView = false;
    this.isAddView = false;
  }

  ngOnChanges() {
    this.form.reset();
    this.isDetailsView = true;
    this.isAddView = false;
    if(this.ItemId != undefined){
      this.toDoListService.getToDoListItemDetails(this.ItemId).subscribe((res:TodoDetails)=>{ 
        this.item = res;
      });
    }    
  }
  AddNewToDoList(){
    this.isDetailsView = false;
    this.isAddView = true;
  }
  onSubmit(){
   if(this.form.invalid) {
    this.form.get('name')?.markAllAsTouched();
    return false;
   }
   else{
     var addEntity = new ToDoListItem();
     addEntity.name = this.form.get('name')?.value;
     addEntity.isCompleted = false;

    this.toDoListService.AddToDoList(addEntity).subscribe((res:ToDoListItem)=>{ 
      this.createdEntity = res;
      this.isDetailsView = true;
      this.isAddView = false;
      this.todolistShareService.addItem(res);
    });
     return true
   }
  }
  UpdateToDoList(){
    this.toDoListService.UpdateToDoList(this.ItemId).subscribe((res:TodoDetails)=>{ 
      this.item = res;
      this.isDetailsView = true;
      this.isAddView = false;
    });
  }

}
