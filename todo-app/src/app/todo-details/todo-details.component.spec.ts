import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TodoDetailsComponent } from './todo-details.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TodolistService } from '../todolist.service';
import { ReactiveFormsModule } from '@angular/forms';


describe('TodoDetailsComponent', () => {
  let component: TodoDetailsComponent;
  let fixture: ComponentFixture<TodoDetailsComponent>;
  let service: TodolistService;
  let httpClient : HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,RouterTestingModule,HttpClientModule,ReactiveFormsModule ],
      declarations: [ TodoDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    service = TestBed.inject(TodolistService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    fixture = TestBed.createComponent(TodoDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
