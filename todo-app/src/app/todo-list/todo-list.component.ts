import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TodolistService } from '../todolist.service';
import { TodolistShareService } from '../todolist-share.service';
TodolistService
@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  @Output() ItemId = new EventEmitter();
   ToDoListItems: any[] = [];
  constructor(private toDoListService:TodolistService,private todolistShareService:TodolistShareService) { }

  ngOnInit() {
    this.toDoListService.getAllToDoList().subscribe((res:any[])=>{ 
      res.forEach(element => {
        this.todolistShareService.addItem(element); 
      });
      this.ToDoListItems = this.todolistShareService.getItems();;
    });
  }
  
  GetItemId(value: string){
   this.ItemId.emit(value)
  }
}
