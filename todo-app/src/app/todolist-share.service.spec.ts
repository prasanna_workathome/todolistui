import { TestBed } from '@angular/core/testing';

import { TodolistShareService } from './todolist-share.service';

describe('TodolistShareService', () => {
  let service: TodolistShareService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TodolistShareService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
