export class TodoDetails {
    id!: string
    name!: string
    isCompleted!: boolean
}
