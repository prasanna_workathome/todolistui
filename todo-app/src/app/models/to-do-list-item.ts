
export class ToDoListItem {
    id!: string;
    name!: string;
    isCompleted: boolean = false;
    createdById!: string;
    createdOn!: Date;
    modifiedById!: string;
    modifiedOn!: Date;
}
