import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TodolistShareService{
  private _items:any[] = [];
  constructor() { }

  addItem(item: any) {
      this._items.push(item);
    }

    getItems(): any[] {
      return this._items;
  }
}
