import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpErrorResponse  } from '@angular/common/http';
import { ToDoListItem } from './models/to-do-list-item';
import { TodoDetails } from './models/todo-details';
import { Observable } from 'rxjs/internal/Observable';
import {  throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodolistService {
  private apiURL: string = 'https://homewebapi.azurewebsites.net/api/ToDoList';
  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  }
  ROOT_URl: any;
  constructor(private httpClient: HttpClient) {   
  }
  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getAllToDoList(){
    return this.httpClient.get<ToDoListItem[]>(`${this.apiURL}/GetAllToDoList`).pipe(catchError(this.handleError));
  }
  public getToDoListItemDetails(id:string) : Observable<TodoDetails>{
    return this.httpClient.get<TodoDetails>(`${this.apiURL}/GetToDoListDetails?itemid=${id}`).pipe(catchError(this.handleError));
  }
  public AddToDoList(toDoList : ToDoListItem){
    return this.httpClient.post<ToDoListItem>(`${this.apiURL}/CreateToDoList`,toDoList,this.httpOptions).pipe(catchError(this.handleError));
  }
  public UpdateToDoList(id:string):  Observable<TodoDetails>{
    return this.httpClient.put<TodoDetails>(`${this.apiURL}/CompleteToDoList/${id}`,null).pipe(catchError(this.handleError));
  }

}
